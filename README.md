# Open Election & Voting Specification
Entities are free to implement however they wish, so long as they meet the specification. This is not the code, just the specification. This is modeled after how the web was built, with RFCs (Request for Comments). Consider this an RFC for elections and voting. 

1. Voting software MUST use open-source software that generates a paper ballot with an auditable paper trail for 20 years
1. Voting hardware SHOULD be open-source (display, input device (if non-touch), computer, printer)
1. Voting software MUST be developed in a public manner and have individual authorship for each line of code contributed. (e.g. GitLab)
1. Voting software MUST consider all contributions submitted (e.g. Merge/Pull Requests)
1. All voting hardware/software MUST display the current checksum of the binary executable on the system AND that checksum MUST be printed on the ballot
1. All eligible voters MUST be registered automatically
1. Voting SHOULD take place on weekends OR take place on a Friday AND Saturday
1. The candidate with the most votes MUST win the election
1. Ballots MUST list all valid candidates in a random order (so as not to give more visibility to any one candidate)
1. All candidates & parties MUST be allowed equal and sufficient debate time
1. All debates MUST be streamed/aired/broadcast on the internet via a public government website with which there no commercial interests or advertising so that there are not conflicts of interest
  1. There MUST be free licensing for ANY network to broadcast so long as there is no advertising whatsoever during the broadcast
  2. The debate footage will be licensed under creative commons license or better
3. New versions of this specification will be voted on every X years